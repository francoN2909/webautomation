# webAutomation

Repository for the web automation completed, as part of the practical test

The web automation took me about 2 hours to complete. I am unfortunately not too brushed up on my mobile automation knowledge, as at my current job I am more focussed on web automation. I only did the basic training for mobile automation, when I first started at dvt. I am unfortunately not able to set up the automation environment for mobile testing. I have however managed mobile applications as a team lead and I am comfortable working with the tests after the set up has been completed.
I have tried doing some research into Mobile automation and the setup, but I was unable to implement it myself. As for API testing, I have never worked with APIs and I saw it a bit of a challenge to learn. I have once again done some reasearch, but I was not able to implement the task as specified in the assessment. 
I am an eager learner and see both of these tasks which I have not completed as a learning experience. I am unfortunately more comfortable and trained in web automation, so I was only able to complete the assessment as such.

I have completed a short automation process demonstrating some functionality on the 'http://the-internet.herokuapp.com/' application.

I completed the automation using Netbeans as an IDE, as I am most familiar with the functionality.

The test can be kicked off using the 'ExecuteClass' within the project.

At the the end of the test a file is created outputting some of the extracted data.

Thank you for you consideration.