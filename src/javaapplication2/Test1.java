/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author niena
 */
public class Test1 {

    PageObjects page = new PageObjects();

    WebDriver driver = new ChromeDriver();

    public boolean doStuff() {
        //Sets all the properties for the driver and then navigates to the specified website
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver.manage().deleteAllCookies();

        driver.manage().window().maximize();
        // Navigates to URL 
        driver.navigate().to(page.webURL());
        //Completes the 'Form Authentication' process
        driver.findElement(By.xpath(page.formAuthLink())).click();

        driver.findElement(By.xpath(page.usernameField())).sendKeys("tomsmith");

        driver.findElement(By.xpath(page.passwordField())).sendKeys("SuperSecretPassword!");

        driver.findElement(By.xpath(page.loginButton())).click();

        try {
            driver.findElement(By.xpath(page.validateLogin())).wait();
        } catch (Exception e) {
            System.out.println("Failed to wait for login validate " + e.toString());
        }
        //Validates login was sucessfull      
        System.out.println("Successfully validated login");

        driver.findElement(By.xpath(page.logout())).click();

        driver.navigate().to(page.webURL());
        //Completes the 'Checkboxes' process
        driver.findElement(By.xpath(page.checkBoxesLink())).click();

        driver.findElement(By.xpath(page.checkBoxUnChecked())).click();

        driver.findElement(By.xpath(page.checkBoxChecked())).click();

        driver.findElement(By.xpath(page.checkBoxChecked())).click();

        driver.navigate().back();
        //Completes the 'DropDown' process        
        driver.findElement(By.xpath(page.dropDownLink())).click();

        driver.findElement(By.xpath(page.dropDownList())).click();

        driver.findElement(By.xpath(page.dropDownListOption1())).click();
        
        driver.findElement(By.xpath(page.dropDownList())).click();

        driver.findElement(By.xpath(page.dropDownList())).click();

        driver.findElement(By.xpath(page.dropDownListOption2())).click();
        
        driver.findElement(By.xpath(page.dropDownList())).click();

        driver.navigate().back();
        //Extracts and outputs all the links on the home page to a text file        
        List<WebElement> exampleList = driver.findElements(By.xpath(page.availableExamplesList()));

        List<String> elementText = new ArrayList<>();
        //Iterates through the List of web elements and outputs them in a formatted manner
        try {

            for (WebElement value : exampleList) {
                elementText.add(value.getText());
            }
        } catch (Exception e) {
            System.out.println("Error with retrieving list");
        }

        int counter = 1;
        for (String temp : elementText) {

            System.out.println("Example " + counter + ":" + temp);
            counter++;
        }
        //Writes console output to a text file 
        writeToLogFile(elementText);
        System.out.println("Successfully executed test");
        //Closes the webdriver        
        driver.close();
        return true;
    }

    public void writeToLogFile(List<String> elementText) {
        try {
            int count = 1;
            PrintWriter writer = new PrintWriter("HomePageLinks.txt");

            for (String headlineIndividual : elementText) {

                writer.println("Example " + count + ":" + headlineIndividual);
                count++;
            }

            writer.close();

        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println("Failed to create text file" + fileNotFoundException.toString());
        }

        System.out.println("Successfully wrote to log file");

    }

}
