/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author niena
 */
public class PageObjects {

    public String webURL() {
        return "http://the-internet.herokuapp.com/";
    }

      public String availableExamplesList() {
        return "//li//a[contains(@href, '')]";
    }

    public String formAuthLink() {
        return "//a[text()= 'Form Authentication']";
    }

    public String checkBoxesLink() {
        return "//a[text()= 'Checkboxes']";
    }

    public String checkBoxChecked() {
        return "//input[@type = 'checkbox'][@checked]";
    }

    public String checkBoxUnChecked() {
        return "//input[@type = 'checkbox'][not(@checked)]";
    }
    
     public String dropDownLink() {
        return "//a[text() = 'Dropdown']";
    }

    public String dropDownList() {
        return "//select[@id = 'dropdown']";
    }

    public String dropDownListOption1() {
        return "//option[@value = '1'][text() = 'Option 1']";
    }

    public String dropDownListOption2() {
        return "//option[@value = '2'][text() = 'Option 2']";
    }

    public String usernameField() {
        return "//input[@type ='text'][@id = 'username']";
    }

    public String passwordField() {
        return "//input[@type ='password'][@id = 'password']";
    }

    public String loginButton() {
        return "//button[@type = 'submit']";
    }

    public String validateLogin() {
        return "//div[contains(text(), 'You logged into a secure area!')]";
    }

    public String logout() {
        return "//i[text() = ' Logout']";
    }
}
